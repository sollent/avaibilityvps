<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180917180505 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE facilitator_working_time (id INT AUTO_INCREMENT NOT NULL, working_time_begin TIME NOT NULL, working_time_end TIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE facilitator (id INT AUTO_INCREMENT NOT NULL, working_time_id INT NOT NULL, days_of_week_id INT NOT NULL, first_name VARCHAR(150) NOT NULL, last_name VARCHAR(180) NOT NULL, UNIQUE INDEX UNIQ_A43A8DB6809A7F7B (working_time_id), UNIQUE INDEX UNIQ_A43A8DB65D20E35A (days_of_week_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE facilitator_days_of_week (id INT AUTO_INCREMENT NOT NULL, monday TINYINT(1) NOT NULL, tuesday TINYINT(1) NOT NULL, wednesday TINYINT(1) NOT NULL, thursday TINYINT(1) NOT NULL, friday TINYINT(1) NOT NULL, saturday TINYINT(1) NOT NULL, sunday TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE facilitator_exceptions (id INT AUTO_INCREMENT NOT NULL, facilitator_id INT NOT NULL, date_begin DATE NOT NULL, date_end DATE NOT NULL, is_working TINYINT(1) NOT NULL, working_time_begin TIME NOT NULL, working_time_end TIME NOT NULL, INDEX IDX_7AE47E53EFB37882 (facilitator_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE facilitator ADD CONSTRAINT FK_A43A8DB6809A7F7B FOREIGN KEY (working_time_id) REFERENCES facilitator_days_of_week (id)');
        $this->addSql('ALTER TABLE facilitator ADD CONSTRAINT FK_A43A8DB65D20E35A FOREIGN KEY (days_of_week_id) REFERENCES facilitator_working_time (id)');
        $this->addSql('ALTER TABLE facilitator_exceptions ADD CONSTRAINT FK_7AE47E53EFB37882 FOREIGN KEY (facilitator_id) REFERENCES facilitator (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE facilitator DROP FOREIGN KEY FK_A43A8DB65D20E35A');
        $this->addSql('ALTER TABLE facilitator_exceptions DROP FOREIGN KEY FK_7AE47E53EFB37882');
        $this->addSql('ALTER TABLE facilitator DROP FOREIGN KEY FK_A43A8DB6809A7F7B');
        $this->addSql('DROP TABLE facilitator_working_time');
        $this->addSql('DROP TABLE facilitator');
        $this->addSql('DROP TABLE facilitator_days_of_week');
        $this->addSql('DROP TABLE facilitator_exceptions');
    }
}
