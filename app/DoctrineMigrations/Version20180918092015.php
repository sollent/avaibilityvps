<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180918092015 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE facilitator DROP FOREIGN KEY FK_A43A8DB65D20E35A');
        $this->addSql('ALTER TABLE facilitator DROP FOREIGN KEY FK_A43A8DB6809A7F7B');
        $this->addSql('ALTER TABLE facilitator ADD CONSTRAINT FK_A43A8DB65D20E35A FOREIGN KEY (days_of_week_id) REFERENCES facilitator_days_of_week (id)');
        $this->addSql('ALTER TABLE facilitator ADD CONSTRAINT FK_A43A8DB6809A7F7B FOREIGN KEY (working_time_id) REFERENCES facilitator_working_time (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE facilitator DROP FOREIGN KEY FK_A43A8DB6809A7F7B');
        $this->addSql('ALTER TABLE facilitator DROP FOREIGN KEY FK_A43A8DB65D20E35A');
        $this->addSql('ALTER TABLE facilitator ADD CONSTRAINT FK_A43A8DB6809A7F7B FOREIGN KEY (working_time_id) REFERENCES facilitator_days_of_week (id)');
        $this->addSql('ALTER TABLE facilitator ADD CONSTRAINT FK_A43A8DB65D20E35A FOREIGN KEY (days_of_week_id) REFERENCES facilitator_working_time (id)');
    }
}
