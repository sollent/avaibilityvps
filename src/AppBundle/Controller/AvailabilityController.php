<?php

namespace AppBundle\Controller;

use AppBundle\Service\GetAvailabilityService;
use AppBundle\Service\UpdateAvailabilityService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @package AppBundle\Controller
 * @Route("/availability")
 */
class AvailabilityController extends Controller
{

    /**
     * @var GetAvailabilityService
     */
    private $availabilityService;

    /**
     * @var UpdateAvailabilityService
     */
    private $updateAvailabilityService;

    /**
     * AvailabilityController constructor.
     * @param GetAvailabilityService $availabilityService
     * @param UpdateAvailabilityService $updateAvailabilityService
     */
    public function __construct(
        GetAvailabilityService $availabilityService,
        UpdateAvailabilityService $updateAvailabilityService
    ) {
        $this->availabilityService = $availabilityService;
        $this->updateAvailabilityService = $updateAvailabilityService;
    }


    /**
     * @Route("/schedule/{facilitator_id}", methods={"GET", "OPTIONS"},
     * name="index-action")
     * @param int $facilitator_id
     * @return JsonResponse
     */
    public function indexAction(int $facilitator_id): JsonResponse
    {
        if ($facilitator_id < 0) {
            return JsonResponse::fromJsonString(json_encode(["Facilitators's id should be great than 0"], true));
        }

        $availabilityJson = $this->availabilityService->getFacilitatorAvailabilityAsJson($facilitator_id);

        $response = new JsonResponse();

        $response->setContent($availabilityJson);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'GET,HEAD,PUT');
        $response->headers->set('Access-Control-Allow-Headers', 'Content-Type');

        return $response;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/schedule/{facilitator_id}", name="post-action", methods={"POST", "PUT"})
     */
    public function postAction(Request $request): JsonResponse
    {

        $facilitator_json = $request->getContent();
        $facilitatorAvailability = json_decode($facilitator_json, true);


        $resultAsJson = $this->updateAvailabilityService->updateFacilitator($facilitatorAvailability);

        $response = new JsonResponse();
        $response->headers->set('Access-Control-Allow-Origin', '*');

        if ($resultAsJson === true) {
            $resultAsJson = json_encode(['message' => 'All data has been update'], true);
        } else {
            $response->setStatusCode(400);
        }

        $response->setContent($resultAsJson);

        return $response;
    }

}