<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * Class Facilitator
 * @package AppBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(name="facilitator", indexes={@Index(name="id_idx", columns={"id"})})
 */
class Facilitator
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="first_name", type="string", length=150, nullable=false)
     */
    private $firstName;

    /**
     * @var string
     * @ORM\Column(name="last_name", type="string", length=180, nullable=false)
     */
    private $lastName;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\FacilitatorWorkingTime")
     * @ORM\JoinColumn(name="working_time_id", referencedColumnName="id", nullable=false)
     */
    private $workingTime;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\FacilitatorDaysOfWeek")
     * @ORM\JoinColumn(name="days_of_week_id", referencedColumnName="id", nullable=false)
     */
    private $daysOfWeek;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Facilitator
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return Facilitator
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return Facilitator
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWorkingTime()
    {
        return $this->workingTime;
    }

    /**
     * @param mixed $workingTime
     * @return Facilitator
     */
    public function setWorkingTime($workingTime)
    {
        $this->workingTime = $workingTime;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDaysOfWeek()
    {
        return $this->daysOfWeek;
    }

    /**
     * @param mixed $daysOfWeek
     * @return Facilitator
     */
    public function setDaysOfWeek($daysOfWeek)
    {
        $this->daysOfWeek = $daysOfWeek;
        return $this;
    }

}