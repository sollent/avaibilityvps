<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class FacilitatorDaysOfWeek
 * @package AppBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(name="facilitator_days_of_week", indexes={@Index(name="id_idx", columns={"id"})})
 */
class FacilitatorDaysOfWeek
{

    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @var boolean
     * @ORM\Column(name="monday", type="boolean")
     * @Assert\Type(type="boolean", message="This value should be of type boolean.")
     */
    private $monday;

    /**
     * @var boolean
     * @ORM\Column(name="tuesday", type="boolean")
     * @Assert\Type(type="boolean", message="This value should be of type boolean.")
     */
    private $tuesday;


    /**
     * @var boolean
     * @ORM\Column(name="wednesday", type="boolean")
     * @Assert\Type(type="boolean", message="This value should be of type boolean.")
     */
    private $wednesday;


    /**
     * @var boolean
     * @ORM\Column(name="thursday", type="boolean")
     * @Assert\Type(type="boolean", message="This value should be of type boolean.")
     */
    private $thursday;

    /**
     * @var boolean
     * @ORM\Column(name="friday", type="boolean")
     * @Assert\Type(type="boolean", message="This value should be of type boolean.")
     */
    private $friday;

    /**
     * @var boolean
     * @ORM\Column(name="saturday", type="boolean")
     * @Assert\Type(type="boolean", message="This value should be of type boolean.")
     */
    private $saturday;

    /**
     * @var boolean
     * @ORM\Column(name="sunday", type="boolean")
     * @Assert\Type(type="boolean", message="This value should be of type boolean.")
     */
    private $sunday;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return FacilitatorDaysOfWeek
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return bool
     */
    public function isMonday()
    {
        return $this->monday;
    }

    /**
     * @param bool $monday
     * @return FacilitatorDaysOfWeek
     */
    public function setMonday($monday)
    {
        $this->monday = $monday;
        return $this;
    }

    /**
     * @return bool
     */
    public function isTuesday()
    {
        return $this->tuesday;
    }

    /**
     * @param bool $tuesday
     * @return FacilitatorDaysOfWeek
     */
    public function setTuesday($tuesday)
    {
        $this->tuesday = $tuesday;
        return $this;
    }

    /**
     * @return bool
     */
    public function isWednesday()
    {
        return $this->wednesday;
    }

    /**
     * @param bool $wednesday
     * @return FacilitatorDaysOfWeek
     */
    public function setWednesday($wednesday)
    {
        $this->wednesday = $wednesday;
        return $this;
    }

    /**
     * @return bool
     */
    public function isThursday()
    {
        return $this->thursday;
    }

    /**
     * @param bool $thursday
     * @return FacilitatorDaysOfWeek
     */
    public function setThursday($thursday)
    {
        $this->thursday = $thursday;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFriday()
    {
        return $this->friday;
    }

    /**
     * @param bool $friday
     * @return FacilitatorDaysOfWeek
     */
    public function setFriday($friday)
    {
        $this->friday = $friday;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSaturday()
    {
        return $this->saturday;
    }

    /**
     * @param bool $saturday
     * @return FacilitatorDaysOfWeek
     */
    public function setSaturday($saturday)
    {
        $this->saturday = $saturday;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSunday()
    {
        return $this->sunday;
    }

    /**
     * @param bool $sunday
     * @return FacilitatorDaysOfWeek
     */
    public function setSunday($sunday)
    {
        $this->sunday = $sunday;
        return $this;
    }


}