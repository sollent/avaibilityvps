<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class FacilitatorExceptions
 * @package AppBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(name="facilitator_exceptions", indexes={@Index(name="id_idx", columns={"id"})})
 */
class FacilitatorExceptions
{

    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="date_begin", type="date")
     * @Assert\Date()
     */
    public $dateBegin;

    /**
     * @ORM\Column(name="date_end", type="date")
     * @Assert\Date()
     */
    public $dateEnd;

    /**
     * @var boolean
     * @ORM\Column(name="is_working", type="boolean")
     * @Assert\Type(type="boolean", message="This value should be of type boolean.")
     */
    private $isWorking;

    /**
     * @ORM\Column(name="working_time_begin", type="time", nullable=true)
     * @Assert\Time()
     */
    private $workingTimeBegin;

    /**
     * @ORM\Column(name="working_time_end", type="time", nullable=true)
     * @Assert\Time()
     */
    private $workingTimeEnd;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Facilitator")
     * @ORM\JoinColumn(name="facilitator_id", referencedColumnName="id", nullable=false)
     */
    private $facilitator;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return FacilitatorExceptions
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateBegin()
    {
        return $this->dateBegin;
    }

    /**
     * @param mixed $dateBegin
     * @return FacilitatorExceptions
     */
    public function setDateBegin($dateBegin)
    {
        $this->dateBegin = new \DateTime($dateBegin);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * @param mixed $dateEnd
     * @return FacilitatorExceptions
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = new \DateTime($dateEnd);
        return $this;
    }

    /**
     * @return bool
     */
    public function isWorking()
    {
        return $this->isWorking;
    }

    /**
     * @param bool $isWorking
     * @return FacilitatorExceptions
     */
    public function setIsWorking($isWorking)
    {
        $this->isWorking = $isWorking;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWorkingTimeBegin()
    {
        return $this->workingTimeBegin;
    }

    /**
     * @param mixed $workingTimeBegin
     * @return FacilitatorExceptions
     */
    public function setWorkingTimeBegin($workingTimeBegin)
    {
        $this->workingTimeBegin = new \DateTime($workingTimeBegin);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWorkingTimeEnd()
    {
        return $this->workingTimeEnd;
    }

    /**
     * @param mixed $workingTimeEnd
     * @return FacilitatorExceptions
     */
    public function setWorkingTimeEnd($workingTimeEnd)
    {
        $this->workingTimeEnd = new \DateTime($workingTimeEnd);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFacilitator()
    {
        return $this->facilitator;
    }

    /**
     * @param mixed $facilitator
     * @return FacilitatorExceptions
     */
    public function setFacilitator($facilitator)
    {
        $this->facilitator = $facilitator;
        return $this;
    }


}