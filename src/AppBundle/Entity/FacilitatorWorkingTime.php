<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class FacilitatorWorkingTime
 * @package AppBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(name="facilitator_working_time", indexes={@Index(name="id_idx", columns={"id"})})
 */
class FacilitatorWorkingTime
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;


    /**
     * @ORM\Column(name="working_time_begin", type="time")
     * @Assert\Time(message="This value is not a valid time. This value must have format HH:MM:SS")
     */
    public $workingTimeBegin;

    /**
     * @ORM\Column(name="working_time_end", type="time")
     * @Assert\Time(message="This value is not a valid time. This value must have format HH:MM:SS")
     */
    public $workingTimeEnd;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return FacilitatorWorkingTime
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWorkingTimeBegin()
    {
        return $this->workingTimeBegin;
    }

    /**
     * @param mixed $workingTimeBegin
     * @return FacilitatorWorkingTime
     */
    public function setWorkingTimeBegin($workingTimeBegin)
    {
        $this->workingTimeBegin = new \DateTime($workingTimeBegin);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWorkingTimeEnd()
    {
        return $this->workingTimeEnd;
    }

    /**
     * @param mixed $workingTimeEnd
     * @return FacilitatorWorkingTime
     */
    public function setWorkingTimeEnd($workingTimeEnd)
    {
        $this->workingTimeEnd = new \DateTime($workingTimeEnd);
        return $this;
    }


}