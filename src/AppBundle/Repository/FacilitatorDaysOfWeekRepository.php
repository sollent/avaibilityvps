<?php

namespace AppBundle\Repository;

use AppBundle\Entity\FacilitatorDaysOfWeek;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class FacilitatorDaysOfWeekRepository
 * @package AppBundle\Repository
 */
class FacilitatorDaysOfWeekRepository
{
    /**
     * @var
     */
    private $repository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * FacilitatorDaysOfWeekRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(FacilitatorDaysOfWeek::class);
        $this->entityManager = $entityManager;
    }

    /**
     * @param $id
     * @return FacilitatorDaysOfWeek|null
     */
    public function findDaysOfWeekById($id): ?FacilitatorDaysOfWeek
    {
        return $this->repository->find($id);
    }

    /**
     * @return void
     */
    public function save(): void
    {
        $this->entityManager->flush();
    }

}