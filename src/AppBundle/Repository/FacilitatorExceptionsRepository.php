<?php

namespace AppBundle\Repository;

use AppBundle\Entity\FacilitatorExceptions;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

/**
 * Class FacilitatorExceptionsRepository
 * @package AppBundle\Repository
 */
class FacilitatorExceptionsRepository
{
    /**
     * @var
     */
    private $repository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * FacilitatorExceptionsRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(FacilitatorExceptions::class);
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $id
     * @return mixed|null
     */

    public function findExceptionById(int $id): ?FacilitatorExceptions
    {
        return $this->repository->find($id);
    }

    /**
     * @param int $facilitator_id
     * @return array|null
     */
    public function findFacilitatorExceptions(int $facilitator_id): ?array
    {
        $exceptions = $this->entityManager
            ->createQueryBuilder()
            ->select('e.id', 'e.dateBegin', 'e.dateEnd', 'e.isWorking', 'e.workingTimeBegin', 'e.workingTimeEnd')
            ->from('AppBundle\Entity\FacilitatorExceptions', 'e')
            ->where('e.facilitator = :id')
            ->setParameter('id', $facilitator_id);

        try {
            return $exceptions->getQuery()->getResult();
        } catch (NoResultException $e) {
            return null;
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @return void
     */
    public function save(): void
    {
        $this->entityManager->flush();
    }

    /**
     * @param FacilitatorExceptions $facilitatorExceptions
     */
    public function create(FacilitatorExceptions $facilitatorExceptions): void
    {
        $this->entityManager->persist($facilitatorExceptions);
        $this->entityManager->flush();
    }

    /**
     * @param FacilitatorExceptions $facilitatorExceptions
     */
    public function delete(FacilitatorExceptions $facilitatorExceptions): void
    {
        $this->entityManager->remove($facilitatorExceptions);
        $this->entityManager->flush();
    }

}

