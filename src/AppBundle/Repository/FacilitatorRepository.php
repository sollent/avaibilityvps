<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Facilitator;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

/**
 * Class FacilitatorRepository
 * @package AppBundle\Repository
 */
class FacilitatorRepository
{
    /**
     * @var
     */
    private $repository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * FacilitatorRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(Facilitator::class);
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $id
     * @return Facilitator|null
     */
    public function findFacilitatorById(int $id): ?Facilitator
    {
        return $this->repository->find($id);
    }

    /**
     * @param $facilitator_id
     * @return Facilitator
     */
    public function findFacilitatorAvailability(int $facilitator_id): ?Facilitator
    {
        $facilitatorAvailability = $this->entityManager
            ->createQueryBuilder()
            ->select('f', 'wt', 'dw')
            ->from('AppBundle\Entity\Facilitator', 'f')
            ->join('f.workingTime', 'wt')
            ->join('f.daysOfWeek', 'dw')
            ->where('f.id = :id')
            ->setParameter('id', $facilitator_id);

        try {
            return $facilitatorAvailability->getQuery()->getSingleResult();
        } catch (NonUniqueResultException $exception) {
            return null;
        } catch (NoResultException $e) {
            return null;
        }
    }


    /**
     * @param string $entityName
     * @return array
     */
    public function getEntityFieldsName(string $entityName)
    {
        return $this->entityManager->getClassMetadata('AppBundle:' . $entityName)->getFieldNames();
    }

}