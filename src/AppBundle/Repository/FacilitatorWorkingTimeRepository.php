<?php

namespace AppBundle\Repository;

use AppBundle\Entity\FacilitatorWorkingTime;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class FacilitatorWorkingTimeRepository
 * @package AppBundle\Repository
 */
class FacilitatorWorkingTimeRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var
     */
    private $repository;

    /**
     * FacilitatorWorkingTimeRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(FacilitatorWorkingTime::class);
        $this->entityManager = $entityManager;
    }

    /**
     * @param $id
     * @return FacilitatorWorkingTime|null
     */
    public function findWorkingTimeById($id): ?FacilitatorWorkingTime
    {
        return $this->repository->find($id);
    }

    /**
     * @return void
     */
    public function save(): void
    {
        $this->entityManager->flush();
    }

}