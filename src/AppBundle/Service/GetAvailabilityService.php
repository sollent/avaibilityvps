<?php

namespace AppBundle\Service;

use AppBundle\Entity\Facilitator;
use AppBundle\Repository\FacilitatorDaysOfWeekRepository;
use AppBundle\Repository\FacilitatorExceptionsRepository;
use AppBundle\Repository\FacilitatorRepository;
use AppBundle\Repository\FacilitatorWorkingTimeRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class GetAvailabilityService
 * @package AppBundle\Service
 */
class GetAvailabilityService
{

    /**
     * @var FacilitatorRepository
     */
    private $facilitatorRepository;

    /**
     * @var FacilitatorExceptionsRepository
     */
    private $facilitatorExceptionsRepository;

    /**
     * @var FacilitatorWorkingTimeRepository
     */
    private $facilitatorWorkingTimeRepository;

    /**
     * @var FacilitatorDaysOfWeekRepository
     */
    private $facilitatorDaysOfWeekRepository;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * AvailabilityService constructor.
     * @param FacilitatorRepository $facilitatorRepository
     * @param FacilitatorExceptionsRepository $facilitatorExceptionsRepository
     * @param FacilitatorWorkingTimeRepository $facilitatorWorkingTimeRepository
     * @param FacilitatorDaysOfWeekRepository $facilitatorDaysOfWeekRepository
     * @param ValidatorInterface $validator
     */
    public function __construct(
        FacilitatorRepository $facilitatorRepository,
        FacilitatorExceptionsRepository $facilitatorExceptionsRepository,
        FacilitatorWorkingTimeRepository $facilitatorWorkingTimeRepository,
        FacilitatorDaysOfWeekRepository $facilitatorDaysOfWeekRepository,
        ValidatorInterface $validator
    ) {
        $this->facilitatorRepository = $facilitatorRepository;
        $this->facilitatorExceptionsRepository = $facilitatorExceptionsRepository;
        $this->facilitatorWorkingTimeRepository = $facilitatorWorkingTimeRepository;
        $this->facilitatorDaysOfWeekRepository = $facilitatorDaysOfWeekRepository;
        $this->validator = $validator;
    }


    /**
     * @param int $facilitator_id
     * @return array|bool|float|int|mixed|string|JsonResponse
     */
    public function getFacilitatorAvailabilityAsJson(int $facilitator_id)
    {
        $facilitator = $this->facilitatorRepository->findFacilitatorAvailability($facilitator_id);
        $exceptions = $this->facilitatorExceptionsRepository->findFacilitatorExceptions($facilitator_id);

        if (!$facilitator || !$exceptions) {
            return
                json_encode(["error: No found data in database"], true);
        }

        $facilitator = $this->getNormalizeFacilitator($facilitator, 'H:i:s');
        $exceptions = $this->getNormalizeExceptions($exceptions, 'Y-m-d', 'H:i:s');

        $availabilityJson = $this->getFullJson($facilitator, $exceptions);

        return $availabilityJson;
    }

    /**
     * @param Facilitator $facilitator
     * @param string $workingTimeFormat
     * @return mixed
     */
    private function getNormalizeFacilitator(Facilitator $facilitator, string $workingTimeFormat = 'G:i')
    {
        $facilitator->getWorkingTime()->workingTimeBegin =
            $facilitator->getWorkingTime()->workingTimeBegin->format($workingTimeFormat);
        $facilitator->getWorkingTime()->workingTimeEnd =
            $facilitator->getWorkingTime()->workingTimeEnd->format($workingTimeFormat);

        return $facilitator;
    }

    /**
     * @param array $exceptions
     * @param string $dateFormat
     * @param string|null $workingTimeFormat
     * @return mixed
     */
    private function getNormalizeExceptions(
        ?array $exceptions,
        string $dateFormat = 'm/d/Y',
        string $workingTimeFormat = 'G:i'
    ) {

        for ($j = 0; $j < count($exceptions); $j++) {
            if ($exceptions[$j]['dateBegin'] instanceof \DateTime) {
                $exceptions[$j]['dateBegin'] = $exceptions[$j]['dateBegin']->format($dateFormat);
                $exceptions[$j]['dateEnd'] = $exceptions[$j]['dateEnd']->format($dateFormat);

                if ($exceptions[$j]['workingTimeBegin'] || $exceptions[$j]['workingTimeEnd']) {
                    $exceptions[$j]['workingTimeBegin'] =
                        $exceptions[$j]['workingTimeBegin']->format($workingTimeFormat);
                    $exceptions[$j]['workingTimeEnd'] =
                        $exceptions[$j]['workingTimeEnd']->format($workingTimeFormat);
                }

            } else {
                $exceptions[$j]['dateBegin'] = date($dateFormat, strtotime($exceptions[$j]['dateBegin']));
                $exceptions[$j]['dateEnd'] = date($dateFormat, strtotime($exceptions[$j]['dateEnd']));

                if ($exceptions[$j]['workingTimeBegin'] || $exceptions[$j]['workingTimeEnd']) {
                    $exceptions[$j]['workingTimeBegin'] =
                        date($workingTimeFormat, strtotime($exceptions[$j]['workingTimeBegin']));
                    $exceptions[$j]['workingTimeEnd'] =
                        date($workingTimeFormat, strtotime($exceptions[$j]['workingTimeEnd']));
                }
            }

        }

        return $exceptions;
    }

    /**
     * @param Facilitator $facilitator
     * @param array $exceptions
     * @return array|bool|float|int|mixed|string|JsonResponse
     */
    private function getFullJson(Facilitator $facilitator, ?array $exceptions)
    {
        $facilitator = $this->getSerializer()->normalize($facilitator);
        $exceptions = $this->getSerializer()->normalize($exceptions);

        $facilitator['exceptions'] = $exceptions;

        $facilitator = json_encode($facilitator);
        return $facilitator;
    }

    /**
     * @return Serializer
     */
    private function getSerializer(): Serializer
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        return $serializer = new Serializer($normalizers, $encoders);
    }

}