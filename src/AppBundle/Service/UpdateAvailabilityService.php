<?php

namespace AppBundle\Service;

use AppBundle\Entity\Facilitator;
use AppBundle\Entity\FacilitatorDaysOfWeek;
use AppBundle\Entity\FacilitatorExceptions;
use AppBundle\Entity\FacilitatorWorkingTime;
use AppBundle\Repository\FacilitatorDaysOfWeekRepository;
use AppBundle\Repository\FacilitatorExceptionsRepository;
use AppBundle\Repository\FacilitatorRepository;
use AppBundle\Repository\FacilitatorWorkingTimeRepository;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class UpdateAvailabilityService
 * @package AppBundle\Service
 */
class UpdateAvailabilityService
{

    /**
     * @var FacilitatorRepository
     */
    private $facilitatorRepository;

    /**
     * @var FacilitatorExceptionsRepository
     */
    private $facilitatorExceptionsRepository;

    /**
     * @var FacilitatorWorkingTimeRepository
     */
    private $facilitatorWorkingTimeRepository;

    /**
     * @var FacilitatorDaysOfWeekRepository
     */
    private $facilitatorDaysOfWeekRepository;

    /**
     * @var ValidatorInterface
     */
    private $validator;


    /**
     * AvailabilityService constructor.
     * @param FacilitatorRepository $facilitatorRepository
     * @param FacilitatorExceptionsRepository $facilitatorExceptionsRepository
     * @param FacilitatorWorkingTimeRepository $facilitatorWorkingTimeRepository
     * @param FacilitatorDaysOfWeekRepository $facilitatorDaysOfWeekRepository
     * @param ValidatorInterface $validator
     */
    public function __construct(
        FacilitatorRepository $facilitatorRepository,
        FacilitatorExceptionsRepository $facilitatorExceptionsRepository,
        FacilitatorWorkingTimeRepository $facilitatorWorkingTimeRepository,
        FacilitatorDaysOfWeekRepository $facilitatorDaysOfWeekRepository,
        ValidatorInterface $validator
    ) {
        $this->facilitatorRepository = $facilitatorRepository;
        $this->facilitatorExceptionsRepository = $facilitatorExceptionsRepository;
        $this->facilitatorWorkingTimeRepository = $facilitatorWorkingTimeRepository;
        $this->facilitatorDaysOfWeekRepository = $facilitatorDaysOfWeekRepository;
        $this->validator = $validator;
    }


    /**
     * @param array $availability
     * @return array|bool|string
     */
    public function updateFacilitator(array $availability)
    {

        $errors = $this->updateAllDataAndGetErrors($availability);

        if (empty($errors)) {
            $this->saveAvailability();
            return true;
        } else {
            $errors = json_encode($errors, true);
            return $errors;
        }
    }


    /**
     * @param array $availability
     * @return array
     */
    private function updateAllDataAndGetErrors(array $availability)
    {
        $facilitator = $this->facilitatorRepository->findFacilitatorById($availability['id']);

        $exceptions = array();

        //Logic for handling facilitator's working time
        $facilitatorWorkingTime = $this->facilitatorWorkingTimeRepository
            ->findWorkingTimeById($availability['workingTime']['id']);

        try {
            $this->setWorkingTime($availability['workingTime'], $facilitatorWorkingTime);
        } catch (\Exception $e) {
            $exceptions[] = $e->getMessage();
        }

        //Logic for handling facilitator's days of week
        $facilitatorDaysOfWeek = $this->facilitatorDaysOfWeekRepository
            ->findDaysOfWeekById($availability['daysOfWeek']['id']);
        $this->setDaysOfWeek($availability['daysOfWeek'], $facilitatorDaysOfWeek);
        $fieldNames = $this->facilitatorRepository->getEntityFieldsName('FacilitatorDaysOfWeek');
        if (!empty($this->findValidateErrors($facilitatorDaysOfWeek, $fieldNames))) {
            $exceptions[] = $this->findValidateErrors($facilitatorDaysOfWeek, $fieldNames);
        }

        //Logic for handling facilitator's all exceptions
        foreach ($availability['exceptions'] as $exception) {
            if (array_key_exists('action', $exception) && $exception['action'] == 'delete') {
                $facilitatorExceptionDelete = $this->facilitatorExceptionsRepository
                    ->findExceptionById($exception['id']);

                if ($facilitatorExceptionDelete == null) {
                    $exceptions['errorForExceptionDeleteAction'] = 'In the database there is no results with such id';
                    return $exceptions;
                }

                $this->facilitatorExceptionsRepository->delete($facilitatorExceptionDelete);
                continue;
            }
            if (!array_key_exists('id', $exception)) {
                $facilitatorExceptionNew = new FacilitatorExceptions();

                try {
                    $this->setException($exception, $facilitator, $facilitatorExceptionNew);
                } catch (\Exception $e) {
                    $exceptions['errorForExceptionAddAction'] = $e->getMessage();
                    return $exceptions;
                }

                if (!empty($this->facilitatorExceptionsValidate($facilitatorExceptionNew))) {
                    $exceptions['errorForExceptionAddAction'] = $this
                        ->facilitatorExceptionsValidate($facilitatorExceptionNew);
                    return $exceptions;
                }

                $this->facilitatorExceptionsRepository->create($facilitatorExceptionNew);
                continue;
            }

            $facilitatorException = $this->facilitatorExceptionsRepository
                ->findExceptionById($exception['id']);

            if ($facilitatorException == null) {
                $exceptions['errorForExceptionUpdateAction'] = 'In the database there is no results with such id';
                return $exceptions;
            }

            try {
                $this->setException($exception, $facilitator, $facilitatorException);
            } catch (\Exception $e) {
                $exceptions['errorForExceptionUpdateAction'] = $e->getMessage();
            }

            if (!empty($this->facilitatorExceptionsValidate($facilitatorException))) {
                $exceptions['errorForExceptionUpdateAction'] = $this
                    ->facilitatorExceptionsValidate($facilitatorException);
            }

        }


        return $exceptions;
    }

    /**
     * @param FacilitatorExceptions $facilitatorExceptions
     * @return array
     */
    private function facilitatorExceptionsValidate(FacilitatorExceptions $facilitatorExceptions)
    {
        $errors = array();

        $fieldNames = $this->facilitatorRepository->getEntityFieldsName('FacilitatorExceptions');
        if (!empty($this->findValidateErrors($facilitatorExceptions, $fieldNames))) {
            $errors[] = $this->findValidateErrors($facilitatorExceptions, $fieldNames);
        }

        return $errors;
    }

    /**
     * @param $facilitatorInformation
     * @param array $entityFields
     * @return array
     */
    private function findValidateErrors(
        $facilitatorInformation,
        array $entityFields
    ) {

        $errors = array();

        foreach ($entityFields as $key => $value) {
            if ($value != 'id') {
                $error = $this->validator->validateProperty($facilitatorInformation, $value);
                $error = $this->getSerializer()->normalize($error);

                if ($error) {
                    $fieldName = $error[0]['propertyPath'];
                    $error = $fieldName . ': ' . $error[0]['messageTemplate'];
                    $errors[] = $error;
                }

            }
        }

        return $errors;
    }

    /**
     * @param array $workingTime
     * @param FacilitatorWorkingTime $facilitatorWorkingTime
     */
    private function setWorkingTime(array $workingTime, FacilitatorWorkingTime $facilitatorWorkingTime)
    {
        $facilitatorWorkingTime
            ->setWorkingTimeBegin($workingTime['workingTimeBegin'])
            ->setWorkingTimeEnd($workingTime['workingTimeEnd']);
    }

    /**
     * @param array $daysOfWeek
     * @param FacilitatorDaysOfWeek $facilitatorDaysOfWeek
     */
    private function setDaysOfWeek(array $daysOfWeek, FacilitatorDaysOfWeek $facilitatorDaysOfWeek)
    {
        $facilitatorDaysOfWeek
            ->setMonday($daysOfWeek['monday'])
            ->setTuesday($daysOfWeek['tuesday'])
            ->setWednesday($daysOfWeek['wednesday'])
            ->setThursday($daysOfWeek['thursday'])
            ->setFriday($daysOfWeek['friday'])
            ->setSaturday($daysOfWeek['saturday'])
            ->setSunday($daysOfWeek['sunday']);
    }

    /**
     * @param array $exception
     * @param Facilitator $facilitator
     * @param FacilitatorExceptions $facilitatorException
     */
    private function setException(
        array $exception,
        Facilitator $facilitator,
        FacilitatorExceptions $facilitatorException
    ) {

        if (!$exception['isWorking'] && $exception['workingTimeBegin'] == "" && $exception['workingTimeEnd'] == "") {
            $facilitatorException
                ->setDateBegin($exception['dateBegin'])
                ->setDateEnd($exception['dateEnd'])
                ->setIsWorking($exception['isWorking'])
                ->setFacilitator($facilitator);
        } else {
            $facilitatorException
                ->setDateBegin($exception['dateBegin'])
                ->setDateEnd($exception['dateEnd'])
                ->setIsWorking($exception['isWorking'])
                ->setWorkingTimeBegin($exception['workingTimeBegin'])
                ->setWorkingTimeEnd($exception['workingTimeEnd'])
                ->setFacilitator($facilitator);
        }


    }


    /**
     * @return void
     */
    public function saveAvailability(): void
    {
        $this->facilitatorExceptionsRepository->save();
        $this->facilitatorDaysOfWeekRepository->save();
        $this->facilitatorWorkingTimeRepository->save();
    }


    /**
     * @return Serializer
     */
    private function getSerializer(): Serializer
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        return $serializer = new Serializer($normalizers, $encoders);
    }

}