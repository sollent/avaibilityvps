<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'AppBundle\Controller\AvailabilityController' shared autowired service.

include_once $this->targetDirs[3].'/src/AppBundle/Repository/FacilitatorRepository.php';
include_once $this->targetDirs[3].'/src/AppBundle/Repository/FacilitatorExceptionsRepository.php';
include_once $this->targetDirs[3].'/src/AppBundle/Repository/FacilitatorWorkingTimeRepository.php';
include_once $this->targetDirs[3].'/src/AppBundle/Repository/FacilitatorDaysOfWeekRepository.php';
include_once $this->targetDirs[3].'/src/AppBundle/Service/GetAvailabilityService.php';
include_once $this->targetDirs[3].'/src/AppBundle/Service/UpdateAvailabilityService.php';
include_once $this->targetDirs[3].'/vendor/symfony/symfony/src/Symfony/Component/DependencyInjection/ContainerAwareTrait.php';
include_once $this->targetDirs[3].'/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Controller/ControllerTrait.php';
include_once $this->targetDirs[3].'/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Controller/Controller.php';
include_once $this->targetDirs[3].'/src/AppBundle/Controller/AvailabilityController.php';

$a = ${($_ = isset($this->services['doctrine.orm.default_entity_manager']) ? $this->services['doctrine.orm.default_entity_manager'] : $this->load('getDoctrine_Orm_DefaultEntityManagerService.php')) && false ?: '_'};
$b = ${($_ = isset($this->services['validator']) ? $this->services['validator'] : $this->getValidatorService()) && false ?: '_'};

$c = new \AppBundle\Repository\FacilitatorRepository($a);

$d = new \AppBundle\Repository\FacilitatorExceptionsRepository($a);

$e = new \AppBundle\Repository\FacilitatorWorkingTimeRepository($a);

$f = new \AppBundle\Repository\FacilitatorDaysOfWeekRepository($a);

return $this->services['AppBundle\Controller\AvailabilityController'] = new \AppBundle\Controller\AvailabilityController(new \AppBundle\Service\GetAvailabilityService($c, $d, $e, $f, $b), new \AppBundle\Service\UpdateAvailabilityService($c, $d, $e, $f, $b));
